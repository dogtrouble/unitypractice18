﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	Rigidbody rb;
	public float speed;
    private int count;
    public Text myText;
    public Text winText;
    public int numCollectibles;
    bool isJumping;
    bool canJump;

    public float jumpForce;

    float moveUp;
    float moveHorizontal;
    float moveVertical;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        moveUp = 0f;
        isJumping = false;
        canJump = true;
    }
	
	// Update is called once per frame
	void Update () {
        moveHorizontal = Input.GetAxis("Horizontal");
		moveVertical = Input.GetAxis("Vertical");
        if (Input.GetKeyDown("space") && canJump) {
            isJumping = true;
            canJump = false;
        }
        
    }
	void FixedUpdate () {


        if (isJumping == true) {
            isJumping = false;
            
            rb.AddForce(new Vector3(0f, 100f, 0f) *jumpForce);
            print("Hello");
            
        }
       
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

		rb.AddForce(movement * Time.deltaTime * speed);

	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup")) {
            Destroy(other.gameObject);
            count += 1;
            SetCountText();
            if (count == numCollectibles) {
                ShowWinText();
            }
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        print(collision.gameObject.tag);
        if (collision.gameObject.tag == "Ground") {
            canJump = true;
        }
    }

    void SetCountText() {
        myText.text = "Count: " + count.ToString();
    }
    void ShowWinText() {
        winText.text = "YOU WIN!";
    }
}
